
package net.qrab.minilab;

import java.awt.Point;
import java.awt.Shape;
import java.awt.geom.AffineTransform;
import java.awt.geom.Area;
import java.awt.geom.Path2D;

public final class Common {

  private static final AffineTransform renderXform = new AffineTransform();

  public static Path2D makePath(double[] xs, double[] ys) {
    Path2D myPath = new Path2D.Double();
    myPath.moveTo(xs[0], ys[0]);
    for (int n = 1 ; n < xs.length ; n++) {
      myPath.lineTo(xs[n], ys[n]);
    }
    myPath.closePath();
    return myPath;
  }

  public static Area scaleArea(Shape containingShape, Shape inputShape, double tx, double ty) {
    renderXform.setToIdentity();
    renderXform.scale(Minilab.scale, Minilab.scale);
    renderXform.translate(containingShape.getBounds2D().getX()+tx,containingShape.getBounds2D().getY()+ty);

    Area scaledArea = new Area(inputShape);
    scaledArea.transform(renderXform);

    return scaledArea;
  }

  public static Area scaleArea(Shape containingShape, Shape inputShape, double scale, double tx, double ty) {
    renderXform.setToIdentity();
    renderXform.scale(scale, scale);
    renderXform.translate(containingShape.getBounds2D().getX()+tx,containingShape.getBounds2D().getY()+ty);

    Area scaledArea = new Area(inputShape);
    scaledArea.transform(renderXform);

    return scaledArea;
  }

  public static Area scaleArea(Shape inputShape, double scale, double tx, double ty) {
    renderXform.setToIdentity();
    renderXform.scale(scale, scale);
    renderXform.translate(tx,ty);

    Area scaledArea = new Area(inputShape);
    scaledArea.transform(renderXform);

    return scaledArea;
  }

  
  
  protected static Point normalize(Point p) {
    return normalize((int)p.getX(), (int)p.getY());
  }

  protected static Point normalize(int x, int y) {
    int magnitude = (int)Math.sqrt((x * x) + (y * y));
    int tx = magnitude == 0 ? 0 : x / magnitude;
    int ty = magnitude == 0 ? 0 : y / magnitude;
    return new Point(tx, ty);
  }

  private Common() { }





}
