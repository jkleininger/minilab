package net.qrab.minilab;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;
import java.util.HashSet;

abstract class Node extends Rectangle {

  protected Shape   myShape;
  protected int     eRequired;

  private final HashSet<Node> neighbors     = new HashSet<>(0);
  private final HashSet<Node> connections   = new HashSet<>(0);

  protected Node(int xloc, int yloc) {
    super( xloc, yloc, 1, 1);
  }

  protected static class Blank extends Node {
    protected Blank(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = 0x00;
      myShape   = new Rectangle2D.Double( 0.5-(Minilab.hallWd/2), 0.5-(Minilab.hallWd/2) , Minilab.hallWd, Minilab.hallWd);
    }
  }

  protected static class Room extends Node {
    protected Room(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = 0x00;
      myShape   = new Rectangle2D.Double(0.0, 0.0, 1.0, 1.0);
    }
  }

  protected static class HallH extends Node {
    protected HallH(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = Minilab.W | Minilab.E;
      myShape   = new Rectangle2D.Double(0.0, 0.5-(Minilab.hallWd/2), 1.0, Minilab.hallWd);
    }
  }

  protected static class HallV extends Node {
    protected HallV(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = Minilab.N | Minilab.S;
      myShape   = new Rectangle2D.Double(0.5-(Minilab.hallWd/2), 0.0, Minilab.hallWd, 1.0);
    }
  }

  protected static class HallNW extends Node {
    protected HallNW(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = Minilab.N | Minilab.W;
      myShape   = Common.makePath(new double[] {0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.0,0.0,0.5-(Minilab.hallWd/2)} , new double[] {0.0,0.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2)} );
    }
  }

  protected static class HallNE extends Node {
    protected HallNE(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = Minilab.N | Minilab.W;
      myShape   = Common.makePath(new double[] {0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0,0.5-(Minilab.hallWd/2)} , new double[] {0.0,0.0,0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2)} );
    }
  }

  protected static class HallSE extends Node {
    protected HallSE(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = Minilab.S | Minilab.E;
      myShape   = Common.makePath(new double[] {0.5-(Minilab.hallWd/2),1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2)} , new double[] {0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0} );
    }
  }

  protected static class HallSW extends Node {
    protected HallSW(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = Minilab.S | Minilab.W;
      myShape   = Common.makePath(new double[] {0.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.0} , new double[] {0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2)} );
    }
  }

  protected static class HallNESW extends Node {
    protected HallNESW(int xloc, int yloc) {
      super(xloc,yloc);
      eRequired = Minilab.N | Minilab.E | Minilab.S | Minilab.W;
      myShape   = Common.makePath(new double[] {0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.0,0.0,0.5-(Minilab.hallWd/2)} , new double[] {0.0,0.0,0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2)} );
    }
  }

  protected static class Hall3N extends Node {
    protected Hall3N(int x, int y) {
      super(x,y);
      eRequired = Minilab.W | Minilab.N | Minilab.E;
      myShape   = Common.makePath(new double[] {0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0,0.0,0.0,0.5-(Minilab.hallWd/2)} , new double[] {0.0,0.0,0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2)} );
    }
  }

  protected static class Hall3E extends Node {
    protected Hall3E(int x, int y) {
      super(x,y);
      eRequired = Minilab.N | Minilab.E | Minilab.S;
      myShape   = Common.makePath(new double[] {0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2)} , new double[] {0.0,0.0,0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0} );
    }
  }

  protected static class Hall3S extends Node {
    protected Hall3S(int x, int y) {
      super(x,y);
      eRequired = Minilab.E | Minilab.S | Minilab.W;
      myShape   = Common.makePath(new double[] {0.0,1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.0} , new double[] {0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2)} );
    }
  }

  protected static class Hall3W extends Node {
    protected Hall3W(int x, int y) {
      super(x,y);
      eRequired = Minilab.S | Minilab.W | Minilab.N;
      myShape   = Common.makePath(new double[] {0.5-(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.0,0.0,0.5-(Minilab.hallWd/2)} , new double[] {0.0,0.0,1.0,1.0,0.5+(Minilab.hallWd/2),0.5+(Minilab.hallWd/2),0.5-(Minilab.hallWd/2),0.5-(Minilab.hallWd/2)} );
    }
  }

  public static void setConnected(Node n0, Node n1) {
    n0.connections.add(n1);
    n1.connections.add(n0);
  }

  public boolean isConnected(Node otherNode) {
    return(connections.contains(otherNode));
  }

  public double distance(Node thatNode) {
    return this.getLocation().distance(thatNode.getLocation());
  }

  protected void addNeighbor(Node n) {
    neighbors.add(n);
  }

  protected HashSet<Node> getNeighbors() {
    return neighbors;
  }

  @Override
  public String toString() {
    return this.getClass().getSimpleName() + " @ " + this.getLocation();
  }

  public void update() {
  }

  public void paint(Graphics2D g) {
    g.setColor(Color.GRAY);
    g.fill(Common.scaleArea(myShape, Minilab.scale, this.x, this.y));
    g.setColor(Color.BLACK);
    g.draw(Common.scaleArea(myShape, Minilab.scale, this.x, this.y));
  }


  public static Node matchRequirements(int requirements, int x, int y) {
    Node retNode;

    switch(requirements) {
      case (Minilab.N | Minilab.S):
        retNode = new Node.HallV(x,y);
        break;
      case (Minilab.E | Minilab.W):
        retNode = new Node.HallH(x,y);
        break;
      case (Minilab.N | Minilab.W):
        retNode = new Node.HallNW(x,y);
        break;
      case (Minilab.N | Minilab.E):
        retNode = new Node.HallNE(x,y);
        break;
      case (Minilab.S | Minilab.W):
        retNode = new Node.HallSW(x,y);
        break;
      case (Minilab.S | Minilab.E):
        retNode = new Node.HallSE(x,y);
        break;
      case (Minilab.W | Minilab.N | Minilab.E):
        retNode = new Node.Hall3N(x,y);
        break;
      case (Minilab.N | Minilab.E | Minilab.S):
        retNode = new Node.Hall3E(x,y);
        break;
      case (Minilab.E | Minilab.S | Minilab.W):
        retNode = new Node.Hall3S(x,y);
        break;
      case (Minilab.S | Minilab.W | Minilab.N):
        retNode = new Node.Hall3W(x,y);
        break;
      case (Minilab.N | Minilab.E | Minilab.S | Minilab.W):
        retNode = new Node.HallNESW(x,y);
        break;
      default:
        retNode = new Node.Blank(x,y);
        break;
    }

    return retNode;
  }



  protected static Node randomNode(int x, int y) {
    switch(Minilab.rand.nextInt(13)) {
      case 0:
        return new Node.Blank(x,y);
      case 1:
        return new Node.Room(x,y);
      case 2:
        return new Node.HallH(x,y);
      case 3:
        return new Node.HallV(x,y);
      case 4:
        return new Node.HallNW(x,y);
      case 5:
        return new Node.HallNE(x,y);
      case 6:
        return new Node.HallSE(x,y);
      case 7:
        return new Node.HallSW(x,y);
      case 8:
        return new Node.HallNESW(x,y);
      case 9:
        return new Node.Hall3N(x,y);
      case 10:
        return new Node.Hall3E(x,y);
      case 11:
        return new Node.Hall3S(x,y);
      case 12:
        return new Node.Hall3W(x,y);
      default:
        return new Node.Blank(x,y);
    }



  }


}
