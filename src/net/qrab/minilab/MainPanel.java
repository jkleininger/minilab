

package net.qrab.minilab;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;


class MainPanel extends JPanel {

  private final  Board    myBoard = new Board(2,4, Minilab.boardWd, Minilab.boardHt);
  //private final HexBoard myBoard = new HexBoard();

  public MainPanel() {
    super();

    addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        myBoard.doClick(e);
      }
    });

    this.setFocusable(true);
    this.setPreferredSize(new Dimension(500,500));
    this.setOpaque(true);
    this.setBackground(Color.WHITE);
    this.setVisible(true);
    this.validate();
    
    add(myBoard);

  }

  public void update() {
    myBoard.update();
  }

  @Override
  public void paint(Graphics gfx) {
    update();

    Graphics2D g = (Graphics2D)gfx;
    myBoard.paint(g);
  }

}
