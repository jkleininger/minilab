package net.qrab.minilab;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.concurrent.ConcurrentHashMap;

public class HexBoard extends Rectangle {

  ConcurrentHashMap<Integer,HexNode> nodes = new ConcurrentHashMap<>();

  protected HexBoard() {
    super(5,5,10,10);

    for(int c=0 ; c<this.getWidth() ; c++) {
      for(int r=0 ; r<this.getHeight() ; r++) {
        nodes.put(linearize(c,r), new HexNode(c,r));
      }
    }
  }

  private int linearize(int c, int r) {
    return (r*(int)this.getWidth())+c;
  }

  public void update() {

  }

  public void doClick(MouseEvent e) {

  }


  public void paint(Graphics2D g) {
    for(HexNode hn : nodes.values()) {
      hn.paint(g, this.getBounds2D());
    }
  }




}
