package net.qrab.minilab;

import java.util.Random;
import javax.swing.JFrame;

public class Minilab {

  protected static final int boardWd    = 30;
  protected static final int boardHt    = 20;
  protected static final int numOfRooms = 5;
  protected static double    scale      = 25;

  protected static double    hallWd     = 0.2;

  protected static           Random rand = new Random();

  private static             MainPanel content;
  private static final       JFrame theFrame = new JFrame("Minilab");

  private   static boolean   isRunning = false;

  protected static final int N = 0x08;
  protected static final int E = 0x04;
  protected static final int S = 0x02;
  protected static final int W = 0x01;

  public static void main(String[] args) {
    javax.swing.SwingUtilities.invokeLater(new Runnable() {
       @Override
       public void run() {
          MainPanel myPanel = new MainPanel();
          theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
          theFrame.setContentPane(new MainPanel());
          theFrame.pack();
          theFrame.setVisible(true);
       }
    });

    isRunning = true;
    Thread theThread = new Thread() {
      @Override
      public void run() {
        theLoop();
      }
    };
    theThread.start();
  }

  private static void theLoop() {
    while (isRunning) {
      theFrame.getContentPane().requestFocusInWindow();
      theFrame.getContentPane().repaint();
    }
  }


}

/*

    0    0 0 0 0    0
    1    0 0 0 1    1
    2    0 0 1 0    2
    3    0 0 1 1    3
    4    0 1 0 0    4
    5    0 1 0 1    5
    6    0 1 1 0    6
    7    0 1 1 1    7
    8    1 0 0 0    8
    9    1 0 0 1    9
    A    1 0 1 0   10
    B    1 0 1 1   11
    C    1 1 0 0   12
    D    1 1 0 1   13
    E    1 1 1 0   14
    F    1 1 1 1   15

         N E S W



*/
