package net.qrab.minilab;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JPanel;

public class Board extends JPanel {
  
  protected int width;
  protected int height;
  protected int x,y;

  private final ConcurrentHashMap<Integer,Node> myNodes;

  public Board(int x, int y, int w, int h) {
    //super(x,y,w,h);
    super();
    this.width = w;
    this.height = h;
    this.x = x;
    this.y = y;
    myNodes = new ConcurrentHashMap<>();
    init();
    System.out.println(this.getBounds());
  }

  private void init() {
    for(int n=0 ; n < Minilab.numOfRooms ; n++) {
      int nx = Minilab.rand.nextInt( this.width/2 ) + this.width/4;
      int ny = Minilab.rand.nextInt( this.height/2 ) + this.height/4;
      myNodes.put(linearize(nx,ny), new Node.Room(nx, ny));
    }
  }

  protected void doClick(MouseEvent e) {
    int mc = (int)Math.floor(e.getPoint().x/Minilab.scale);
    int mr = (int)Math.floor(e.getPoint().y/Minilab.scale);
    if(0<=mc && mc<this.width && 0<=mr && mr<this.height) {
      this.positionNodes();
      System.out.println(getNode(mc,mr) + " @ ("+mc+","+mr+")");
    } else {
      RNG(myNodes);
      for(Node n0 : myNodes.values()) {
        for(Node n1 : n0.getNeighbors()) {
          connectNodes(n0,n1);
        }
      }
    }

  }

  private void translateNode(Node theNode, int tx, int ty) {
    if(theNode!=null && this.getNode(x, y)==null) {
      int x0 = theNode.x;
      int y0 = theNode.y;
      int x1 = x0 + tx;
      int y1 = y0 + ty;
      if(this.getNode(x1, y1)==null) {
        theNode.translate(tx, ty);
        myNodes.remove(linearize(x0,y0));
        myNodes.put(linearize(x1,y1), theNode);
      }
    }
  }

  private void connectNodes(Node n0, Node n1) {
    if(!n0.isConnected(n1)) {
      if(n0.x==n1.x) {
        connectNodesVertical(n0,n1);
      } else if(n0.y==n1.y) {
        connectNodesHorizontal(n0,n1);
      } else {
        connectNodesL(n0,n1);
      }
      Node.setConnected(n0,n1);
    }
  }

  private void connectNodesVertical(Node n0, Node n1) {
    if(n0.x!=n1.x) return;

    int x0 = n0.x;
    int y0,y1;

    if(n0.y<n1.y) {
      y0=n0.y;
      y1=n1.y;
    } else if(n1.y<n0.y) {
      y0=n1.y;
      y1=n0.y;
    } else return;

    for(int cy=y0+1 ; cy<y1 ; cy++) {
      safePut(myNodes, new Node.HallV(x0,cy));
    }

  }

  private void connectNodesHorizontal(Node n0, Node n1) {
    if(n0.y!=n1.y) return;

    int y0 = n0.y;
    int x0,x1;

    if(n0.x<n1.x) {
      x0=n0.x;
      x1=n1.x;
    } else if(n1.x<n0.x) {
      x0=n1.x;
      x1=n0.x;
    } else return;

    for(int cx=x0+1 ; cx<x1 ; cx++) {
      safePut(myNodes, new Node.HallH(cx,y0));
    }

  }

  void connectNodesL(Node n0, Node n1) {
    int x0,x1,y0,y1;
    int xmeet, ymeet;
    Node cornerNode;

    if((n0.x>n1.x && n0.y>n1.y) || (n0.x>n1.x && n0.y<n1.y)) {
      x0 = n1.x;
      x1 = n0.x;
      y0 = n1.y;
      y1 = n0.y;
    } else {
      x0 = n0.x;
      x1 = n1.x;
      y0 = n0.y;
      y1 = n1.y;
    }

    if(y0>y1) {
      xmeet = x0;
      ymeet = y1;
      cornerNode = new Node.HallSE(xmeet,ymeet);
    } else {
      xmeet = x1;
      ymeet = y0;
      cornerNode = new Node.HallSW(xmeet,ymeet);
    }

    safePut(myNodes, cornerNode);
    connectNodes( getNode(xmeet,ymeet), getNode(x1,y1) );
    connectNodes( getNode(xmeet,ymeet), getNode(x0,y0) );

  }


  private void safePut(ConcurrentHashMap<Integer,Node> map, Node theNode) {
    if(theNode!=null) {
      int theIndex = linearize(theNode.x, theNode.y);
      if(map.containsKey(theIndex)) {
        int newRequirements = myNodes.get(theIndex).eRequired | theNode.eRequired;
        theNode = Node.matchRequirements(newRequirements, theNode.x, theNode.y);
      }
      myNodes.put(linearize(theNode.x,theNode.y), theNode);

    }
  }


  private Point getCartesian(int n) {
    return new Point( n%(int)this.getWidth() , (int)Math.floor(n/this.getWidth()));
  }

  private int linearize(int c, int r) {
    return (r*(int)this.getWidth())+c;
  }

  protected Node getNode(int c, int r) {
    return myNodes.get( linearize(c,r) );
  }

  protected void RNG(ConcurrentHashMap<Integer,Node> theseNodes) {
    double  pqDistance, pcDistance, qcDistance;
    boolean noGood;
    for(Node thisNode : theseNodes.values()) {
      for(Node thatNode : theseNodes.values()) {
        if(!thisNode.equals(thatNode)) {
          pqDistance = thisNode.distance(thatNode);
          noGood = false;
          for(Node candidateNode : theseNodes.values()) {
            if( !thisNode.equals(candidateNode) && !thatNode.equals(candidateNode) ) {
              pcDistance = candidateNode.distance(thisNode);
              qcDistance = candidateNode.distance(thatNode);
              if( (pcDistance<pqDistance) && (qcDistance<pqDistance) ) {
                noGood=true;
                break;
              }
            }
          }
          if(!noGood) { thisNode.addNeighbor(thatNode); }
        }
      }
    }
  }


  private boolean positionNodes() {
    Point didntMove = new Point(0,0);
    boolean moved = false;
    if(myNodes.values().size()<=1) return false;
    for(Node r : myNodes.values()) {
      Point separation = getSeparation(r);
      Point cohesion = getCohesion(r);
      cohesion.setLocation(0,0);
      if( !separation.equals(didntMove) || !cohesion.equals(didntMove)) {
        moved = true;
        translateNode(r, separation.x , separation.y );
        translateNode(r, cohesion.x   , cohesion.y );
      }
    }
    System.out.println("moved? " + (moved?"yes":"no") );
    return moved;
  }




  private Point getCohesion(Node thisNode) {
    int px = 0;
    int py = 0;
    int neighbors = 0;
    Point retPoint;
    for(Node thatNode : myNodes.values()) {
      if( !thisNode.equals(thatNode) ) {
        px += thatNode.getX();
        py += thatNode.getY();
        neighbors++;
      }
    }
    if(neighbors>0) {
      px /= neighbors;
      py /= neighbors;
    }
    px -= thisNode.getX();
    py -= thisNode.getY();

    retPoint = Common.normalize(px,py);

    Rectangle testRect = nodeHood(thisNode);
    for(Node thatNode : myNodes.values()) {
      if( !thisNode.equals(thatNode) &&
          thatNode.getBounds().intersects(testRect) ) {
        retPoint.setLocation(0,0);
      }
    }
    return retPoint;
  }

  private Rectangle nodeHood(Node n) {
    return new Rectangle(
      n.x-1,
      n.y-1,
      3,
      3
    );
  }


  private Point getSeparation(Node thisNode) {
    int px        = 0;
    int py        = 0;
    for(Node thatNode : myNodes.values()) {
      if( !thisNode.equals(thatNode) ) { //&&
          //nodeHood(thisNode).intersects(nodeHood(thatNode))) {
        px += thatNode.x - thisNode.x;
        py += thatNode.y - thisNode.y;
      }
    }
    return Common.normalize(px,py);
  }


























  protected void update() {
    for(Node n : myNodes.values()) {
      n.update();
    }
  }

  protected void paint(Graphics2D g) {
    
    g.setColor(Color.BLACK);
    g.fill(this.getBounds());
    
    g.setColor(Color.DARK_GRAY);
    g.fill(new Rectangle(
      (int)(this.x * Minilab.scale),
      (int)(this.y * Minilab.scale),
      (int)(this.width * Minilab.scale),
      (int)(this.height * Minilab.scale)
    ));
    g.setColor(Color.BLUE);
    g.draw(new Rectangle(
      (int)(this.x * Minilab.scale),
      (int)(this.y * Minilab.scale),
      (int)(this.width * Minilab.scale),
      (int)(this.height * Minilab.scale)
    ));
    for(Node n : myNodes.values()) {
      n.paint(g);
    }
  }


}
