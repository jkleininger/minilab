package net.qrab.minilab;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Shape;

public class HexNode extends Rectangle {

  Shape  myFillShape;
  Shape  myBorderShape;
  Color  myFill   = Color.ORANGE;
  Color  myBorder = Color.BLACK;
  double borderThickness = 0.15;
  double xpos,ypos;
  double radius;

  public HexNode(int col, int row) {
    super(col,row,1,1);
    radius        = 1;
    myBorderShape = makeHex(radius, col, row);
    myFillShape   = makeHex(radius * (1-borderThickness), col, row);
  }

  private double col2xpos(int c) {
    if(c%2==0) {
      return c;
    } else {
      return c + (3 / Math.sqrt(3));
    }
  }

  private double row2ypos(int r) {
    return r/2;
  }

  private Shape makeHex(double rad, int col, int row) {
    double h = 2 * rad;
    double s = h / Math.sqrt(3);
    double t = rad / Math.sqrt(3);
    double myX = -s/2;
    double myY = -rad;

    xpos = (double)col * (s + t);
    ypos = ((double)row * h) + (col % 2) * (h / 2);

    return Common.makePath(
      new double[] { myX, myX+s, myX+s+t,   myX+s,       myX,         myX-t   },
      new double[] { myY, myY,   myY+rad,   myY+rad+rad, myY+rad+rad, myY+rad }
    );
  }


  public void update() {
  }

  public void paint(Graphics2D g, Shape context) {
    g.setColor(myBorder);
    g.fill(Common.scaleArea(context, myBorderShape, xpos, ypos));
    g.setColor(myFill);
    g.fill(Common.scaleArea(context, myFillShape, xpos, ypos));
  }

}
